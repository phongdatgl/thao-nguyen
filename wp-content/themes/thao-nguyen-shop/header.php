<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Thao Nguyen Shop
 */
?><!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<!-- Basic Page Needs
================================================== -->
<meta charset="<?php bloginfo( 'charset' ); ?>">
<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
</head>
<body class="boxed"><?php //body_class(); ?>
<div id="wrapper">
	<div id="top-bar">
		<div class="container">

			<!-- Top Bar Menu -->
			<div class="ten columns">
				<ul class="top-bar-menu">
					<li><i class="fa fa-phone"></i> 097.6996.876</li>
					<li><i class="fa fa-envelope"></i> <a href="#">nguyencivilnghean@gmail.com</a></li>
				</ul>
			</div>

			<!-- Social Icons -->
			<div class="six columns">
				<ul class="social-icons">
					<li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
				</ul>
			</div>

		</div>
	</div>
	<div class="clearfix"></div>
	<div class="container">


		<!-- Logo -->
		<div class="four columns">
			<div id="logo">
				<h1><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="Cửa Hàng Thảo Nguyên" /></a></h1>
			</div>
		</div>


		<!-- Additional Menu -->
		<div class="twelve columns">
			<div id="additional-menu">
				<ul>
					<li>Hàng thực phẩm chức năng 100% nhập ngoại từ Úc</li>
				</ul>
			</div>
		</div>


		<!-- Shopping Cart -->
		<div class="twelve columns">
			<!-- Search -->
			<nav class="top-search">
				<form action="#" method="get">
					<button><i class="fa fa-search"></i></button>
					<input class="search-field" type="text" placeholder="Tìm kiếm" value=""/>
				</form>
			</nav>

		</div>

	</div>
	<!-- Navigation
	================================================== -->
	<div class="container">
		<div class="sixteen columns">

			<a href="index.html#menu" class="menu-trigger"><i class="fa fa-bars"></i> Menu</a>

			<nav id="navigation">
				<?php wp_nav_menu( array(
		            'menu'              => 'primary',
		            'theme_location'    => 'primary',
		            'depth'             => 2,
		            'container'         => 'div',
		            'container_class'   => 'collapse navbar-collapse',
		            'container_id'      => 'navbar-collapse',
		            'menu_class'        => 'menu',
		            'menu_id'			=>	'responsive',
		            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		            'walker'            => new wp_bootstrap_navwalker())
		        ); ?>
				
			</nav>
		</div>
	</div>


