<?php
/**
 * Thao Nguyen Shop functions and definitions
 *
 * @package Thao Nguyen Shop
 */
//require_once dirname( __FILE__ ) . '/inc/options-framework.php';
require_once dirname( __FILE__ ) . '/inc/wp_bootstrap_navwalker.php';
/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'thao_nguyen_shop_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function thao_nguyen_shop_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Thao Nguyen Shop, use a find and replace
	 * to change 'thao-nguyen-shop' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'thao-nguyen-shop', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'thao-nguyen-shop' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'thao_nguyen_shop_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // thao_nguyen_shop_setup
add_action( 'after_setup_theme', 'thao_nguyen_shop_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function thao_nguyen_shop_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'thao-nguyen-shop' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'thao_nguyen_shop_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function thao_nguyen_shop_scripts() {
	wp_enqueue_style( 'style', get_stylesheet_uri(), array(), NULL);
	//green color for theme
	wp_enqueue_style( 'green-color', get_template_directory_uri() . '/assets/css/colors/green.css', array(), NULL );


	//include navigation
	wp_enqueue_script( 'navigation', get_template_directory_uri() . '/js/navigation.js', array(), NULL, TRUE);
	//include skip link
	wp_enqueue_script( 'skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), NULL, TRUE);
	//include jquery 1.11
	wp_enqueue_script( 'jquery-1.11', get_template_directory_uri() . '/assets/scripts/jquery-1.11.0.min.js', array(), NULL, TRUE);
	//include jquery migrate
	wp_enqueue_script( 'jquery-migrate-1.2.1', get_template_directory_uri() . '/assets/scripts/jquery-migrate-1.2.1.min.js', array(), NULL, TRUE);
	//panel Menu
	wp_enqueue_script( 'panel-menu', get_template_directory_uri() . '/assets/scripts/jquery.jpanelmenu.js', array(), NULL, TRUE);
	//themepunch
	wp_enqueue_script( 'themepunch', get_template_directory_uri() . '/assets/scripts/jquery.themepunch.plugins.min.js', array(), NULL, TRUE);
	//themepunch revolution
	wp_enqueue_script( 'themepunch-revolution', get_template_directory_uri() . '/assets/scripts/jquery.themepunch.revolution.min.js', array(), NULL, TRUE);
	//themepunch showbizpro
	wp_enqueue_script( 'themepunch-showbizpro', get_template_directory_uri() . '/assets/scripts/jquery.themepunch.showbizpro.min.js', array(), NULL, TRUE);
	//hover intent
	wp_enqueue_script( 'hover-intent', get_template_directory_uri() . '/assets/scripts/hoverIntent.js', array(), NULL, TRUE);
	//Super fish
	wp_enqueue_script( 'super-fish', get_template_directory_uri() . '/assets/scripts/superfish.js', array(), NULL, TRUE);
	//custom js
	wp_enqueue_script( 'custom', get_template_directory_uri() . '/assets/scripts/custom.js', array(), NULL, TRUE);
	



	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'thao_nguyen_shop_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
