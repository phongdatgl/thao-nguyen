<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'thaonguyen');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.#Y}U,bLr&Dn++JV}U%Iv4U#6%L7]<,+Iz)e+.XKG$(LWO#wh3qX^JcvWj9_A/ol');
define('SECURE_AUTH_KEY',  'Myt?l$*/!N1OH}^tm/2Odd5d{n9%oLX+O31G:2o4^3j7Y~Q$C:;#{Es#X:-M8_-Q');
define('LOGGED_IN_KEY',    'J-f]NzN)oY)VZ4K:0Q;_;rn4fSZ-[ZF-{i-:+,7&udJ11 E9EQ@M-FKS1;9uV]FN');
define('NONCE_KEY',        'QHuKRL(|9}7q<lj;4-!Em-qpF[aGckcNF]w~svsgbiWSVkVr~?!y7I!(j)=4l^xB');
define('AUTH_SALT',        '8T(a!Ot$-JjB*)gv;6#T~K/6>8J,()kx:aT^*Uh>v[y?|W>1NAF--&|Li{>l/cT;');
define('SECURE_AUTH_SALT', 'SZz}^,Z~wE8C_`8!bWAPjd?,N;SD;t#65t0ni?=?B}>]=Ru:gL}5QOE3q{#&|<Fw');
define('LOGGED_IN_SALT',   '3O~i|-`tONN%o-cG;c^t-}4z;TN|>v5_!D<Vgz$ub@fyL=+`%Q%&PP#=IH,c9V*{');
define('NONCE_SALT',       ')Qcf+lC$.|#[;w^VDL4CZ&iM<X7rb3>R#-X5|$L1>1`9[1H~tOBKPW#iUXjU+8o3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tn_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'vi');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
